const handleActionEdit = (elementId) => {
    const el = document.getElementById(elementId)
    const inputUsername = document.getElementById("inputUsername")
    const inputEmail = document.getElementById("inputEmail")
    const inputPassword = document.getElementById("inputPassword")

    console.log(elementId)
  
    inputUsername.value = el.children[1].innerText
    inputEmail.value = el.children[2].innerText
    inputPassword.value = el.children[3].innerText
  
    document.getElementById("buttonInsert").disabled = true;
    document.getElementById("buttonEdit").disabled = false;
  
    let userId = elementId.split('-')[2]
    document.getElementById("buttonEdit").onclick = () => { handleSubmitEdit(userId) }
    //document.getElementById("buttonEdit").setAttribute('onclick',`handleSubmitEdit(${menuId})`)
  }
  
  // const handleInsertMenu = async () => {
  //   const inputUsername = document.getElementById("inputUsername")
  //   const inputEmail = document.getElementById("inputEmail")
  //   const inputPassword = document.getElementById("inputPassword")
  
  //   const resp = await fetch('http://localhost:7070/api/user', {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify({
  //       name: inputName.value,
  //       description: inputDesc.value,
  //       price: inputPrice.value
  //     })
  //   })
  //   console.log(body)
  //   location.reload()
  // }
  
  const handleInsertMenu = async () => {
    const inputUsername = document.getElementById("inputUsername")
    const inputEmail = document.getElementById("inputEmail")
    const inputPassword = document.getElementById("inputPassword")
    
    //create new URLSearchParams object 
    const data = new URLSearchParams();
    //append key-value pairs
    // data.append('id', 4);
    data.append('username', inputUsername.value);
    data.append('email', inputEmail.value);
    data.append('password', inputPassword.value);
  
    const resp = await fetch('http://localhost:3000/api/user', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: data
      
    })
    console.log(data)
    location.reload()
  }

  const handleInsertUser = async () => {
    const inputUsername = document.getElementById("username")
    const inputEmail = document.getElementById("email")
    const inputPassword = document.getElementById("password")
    
    //create new URLSearchParams object 
    const data = new URLSearchParams();
    //append key-value pairs
    // data.append('id', 4);
    data.append('username', inputUsername.value);
    data.append('email', inputEmail.value);
    data.append('password', inputPassword.value);
  
    const resp = await fetch('http://localhost:3000/auth/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: data
      
    })
    console.log(data)
    res.redirect('/games')
  }
  
  // const handleSubmitEdit = async (menuId) => {
  //   const inputName = document.getElementById("inputName")
  //   const inputDesc = document.getElementById("inputDesc")
  //   const inputPrice = document.getElementById("inputPrice")
  
  //   const resp = await fetch(`http://localhost:7070/menu/${menuId}`, {
  //     method: 'PUT',
  //     headers: {
  //       'Content-Type': 'application/json'
  //     },
  //     body: JSON.stringify({
  //       name: inputName.value,
  //       description: inputDesc.value,
  //       price: inputPrice.value
  //     })
  //   })
  
  //   location.reload()
  // }
  
  const handleSubmitEdit = async (userId) => {
    const inputUsername = document.getElementById("inputUsername")
    const inputEmail = document.getElementById("inputEmail")
    const inputPassword = document.getElementById("inputPassword")
    
    //create new URLSearchParams object 
    const data = new URLSearchParams();
    //append key-value pairs
    data.append('username', inputUsername.value);
    data.append('email', inputEmail.value);
    data.append('password', inputPassword.value);
  
    const resp = await fetch(`http://localhost:3000/api/user/${userId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: data
    })
  
    location.reload()
  }
  
  const handleDelete = async (userId) => {
    const confirmation = confirm('Are you sure?')
    if(confirmation){
      const resp = await fetch(`http://localhost:3000/api/user/${userId}`, {
        method: 'DELETE',
      })
      location.reload()
    }
  }