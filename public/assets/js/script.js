
function getPilihanComputer(){
    const comp = Math.random();
    if( comp < 0.34 ) return 'kertas'; 
    if( comp >= 0.34 && comp < 0.67 ) return 'gunting'; 
    return 'batu'; 
}

function getHasil(comp, player){
    if( player == comp ) return 'seri';
    if( player == 'gunting' ) return ( comp == 'kertas' ) ? 'menang' : 'kalah';
    if( player == 'kertas' ) return ( comp == 'gunting' ) ? 'kalah' : 'menang';
    if( player == 'batu' ) return ( comp == 'gunting' ) ? 'menang' : 'kalah';
    
}

function setHide(){
    document.querySelector('#player-win').setAttribute('class','d-none');
    document.querySelector('#default').setAttribute('class','d-none');
    document.querySelector('#computer-win').setAttribute('class','d-none');
    document.querySelector('#draw').setAttribute('class','d-none');
    document.querySelector('#refresh').setAttribute('class','d-block'); 
}

function setHasil(hasil){
  if (hasil == 'menang') {
    setHide();
    document.querySelector('#player-win').setAttribute('class','d-block bg-success p-5');
  } 
  else if (hasil == 'seri') {
    setHide();
    document.querySelector('#draw').setAttribute('class','d-block bg-success p-5');
  } 
  else if (hasil == 'kalah') {
    setHide();
    document.querySelector('#computer-win').setAttribute('class','d-block bg-success p-5');
  }
}

function setComputer(pilihanComputer){
  if (pilihanComputer == 'kertas') {
    document.querySelector('#comkertas').setAttribute('class','active-com');
    document.querySelector('#comgunting').setAttribute('class','btns-computer');
    document.querySelector('#combatu').setAttribute('class','btns-computer');
  } 
  else if (pilihanComputer == 'gunting') {
    document.querySelector('#comkertas').setAttribute('class','btns-computer');
    document.querySelector('#comgunting').setAttribute('class','active-com');
    document.querySelector('#combatu').setAttribute('class','btns-computer');
  } 
  else if (pilihanComputer == 'batu') {
    document.querySelector('#comkertas').setAttribute('class','btns-computer');
    document.querySelector('#comgunting').setAttribute('class','btns-computer');
    document.querySelector('#combatu').setAttribute('class','active-com');
  }
}

function setFreeze(hasil){
  if (hasil != null){
    document.querySelector('#kertas').classList.add('no-click');
    document.querySelector('#gunting').classList.add('no-click');
    document.querySelector('#batu').classList.add('no-click');
  }
}

function refreshButton(){
  hasil = null;
  document.querySelector('#comkertas').setAttribute('class','btns-computer');
  document.querySelector('#comgunting').setAttribute('class','btns-computer');
  document.querySelector('#combatu').setAttribute('class','btns-computer');
  document.querySelector('#player-win').setAttribute('class','d-none');
  document.querySelector('#default').setAttribute('class','d-block p-5');
  document.querySelector('#computer-win').setAttribute('class','d-none');
  document.querySelector('#draw').setAttribute('class','d-none');
  document.querySelector('#kertas').classList.remove('no-click');
  document.querySelector('#gunting').classList.remove('no-click');
  document.querySelector('#batu').classList.remove('no-click');
  document.querySelector('#kertas').classList.remove('active');
  document.querySelector('#gunting').classList.remove('active');
  document.querySelector('#batu').classList.remove('active');
  document.querySelector('#refresh').setAttribute('class','d-none');
}

// Get all buttons with class="btn" inside the container
var btnContainer = document.getElementById("player");

// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("btn");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");

    // If there's no active class
    if (current.length > 0) { 
      current[0].className = current[0].className.replace("active", "");
    }

    // Add the active class to the current/clicked button
    this.className += " active";
  });
}

const playerChoose = document.querySelectorAll('.btn');

playerChoose.forEach(function(pil){
  pil.addEventListener('click', function(){
    const pilihanComputer = getPilihanComputer(); //kertas
    const pilihanPlayer = pil.id; //gunting
    const hasil = getHasil(pilihanComputer, pilihanPlayer);  //menang

    setHasil(hasil);
    setComputer(pilihanComputer);
    setFreeze(hasil); //not null
    console.log("Pilihan Player: "+pilihanPlayer);
    console.log("Pilihan Computer: "+pilihanComputer);
    console.log("Hasil: "+hasil);

    fetch('/api/userhistory', {
    method: 'POST',
    headers:{
      'Content-Type': 'application/x-www-form-urlencoded'
    },    
    body: new URLSearchParams({
        'user_id': 1,
        'user_choice': pilihanPlayer,
        'computer_choice': pilihanComputer,
        'result': hasil,
      })
    })
    .then(data => {
        console.log('Success:', data);
    })
    .catch((error) => {
        console.error('Error:', error);
    });
});
})

const pRefresh = document.querySelector('#refresh');
pRefresh.addEventListener('click',function(){
    refreshButton();
    
})