import React, {useState} from 'react'
import {Button, Form} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import Players from './Players'
import {v4 as uuid} from 'uuid'
import {Link, useNavigate} from 'react-router-dom'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Sidebar from './Sidebar';

function Add(){

    const[username, setUsername] = useState('')
    const[email, setEmail] = useState('')
    const[experience, setExperience] = useState('')

    let history = useNavigate()

    const handleSubmit = (e) => {
        e.preventDefault()

        const ids = uuid()
        let uniqueId = ids.slice(0,8)

        let a = username,
        b = email,
        c = experience

        Players.push({id:uniqueId, username: a, email: b, experience: c})

        window.alert('Player data submitted successfully');

        history('/player')
    }

    return(
        <div>
            <Container fluid className='width-100 m-0 p-0' style={{backgroundColor:'#ebebeb'}}>
                <Row>
                    <Col md="auto">
                        <Sidebar/>
                    </Col>
                    <Col>
                    <h1 className='ps-5 pt-3 text-start'>Add New Player</h1>
                    <Form className='d-grid gap-2' style={{margin:"3rem"}}>
                        <Form.Group className='mb-1 text-start' controlId='formUsername'>
                            <label htmlFor='username' className='form-label text-left !important' >
                                Username
                            </label>
                            <Form.Control type='text' placeholder='Enter Username' required onChange={(e) => setUsername(e.target.value)} />
                        </Form.Group>
                        <Form.Group className='mb-1 text-start' controlId='formEmail'>
                            <label htmlFor='username' className='form-label text-left !important' >
                                Email
                            </label>
                            <Form.Control type='email' placeholder='Enter Email' required onChange={(e) => setEmail(e.target.value)}>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group className='mb-1 text-start' controlId='formExperience'>
                            <label htmlFor='username' className='form-label text-left !important' >
                                Experience
                            </label>
                            <Form.Control type='text' placeholder='Enter Experience' required onChange={(e) => setExperience(e.target.value)}>
                            </Form.Control>
                        </Form.Group>
                        <Button onClick={(e) => handleSubmit(e)} type='submit'>Submit</Button>
                    </Form>
                    </Col>
                </Row>
            </Container>
            
        </div>
    )

}

export default Add