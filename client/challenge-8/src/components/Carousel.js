import Carousel from 'react-bootstrap/Carousel';

function DarkVariantExample() {
  return (
    <Carousel style={{height:'300px'}} variant="dark">
      <Carousel.Item style={{height:'300px',borderRadius:'10px'}} v>
        <img
          className="d-block w-100"
          src="/jordan.webp"
          alt="First slide"
        />
        <Carousel.Caption style={{backgroundColor:'rgba(255,255,255,0.7)'}}>
          <h5>The Origin of Air Jordan 1 'Chicago'</h5>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item style={{height:'300px'}}>
        <img
          className="d-block w-100"
          src="./lebron2.webp"
          alt="Second slide"
        />
        <Carousel.Caption style={{backgroundColor:'rgba(255,255,255,0.7)'}}>
          <h5>Lebron James pass Kareem as NBA All Time Leading Scorer!</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item style={{height:'300px'}}>
        <img
          className="d-block w-100"
          src="./kyrie.webp"
          alt="Third slide"
        />
        <Carousel.Caption style={{backgroundColor:'rgba(255,255,255,0.4)'}}>
          <h5>Kyrie on being traded to the Dallas Mavericks</h5>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default DarkVariantExample;