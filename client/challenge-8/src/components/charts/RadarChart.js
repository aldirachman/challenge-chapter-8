import {Radar} from 'react-chartjs-2'

import {
    Chart as ChartJS,
    LineElement,
    CategoryScale,
    Tooltip,
    Legend,
    RadialLinearScale,
    Filler
} from 'chart.js'

ChartJS.register(
    LineElement,
    CategoryScale,
    Tooltip,
    Legend,
    RadialLinearScale,
    Filler
)

function RadarChart(){
    const data = {
        labels: ["Januari","Februari","Maret", "April","Mei","Juni"],
        datasets:[{
            label: "Men",
            data: [8,7,6,5,9,7],
            // backgroundColor: '#f5d98c',
            borderColor: '#7ed3ed',
            // pointBorderColor: 'transparent',
            // // pointBorderWidth: 4,
            // // tension: 0.5
        },
        {
            label: "Women",
            data: [7,8,9,8,5,4],
            // backgroundColor: 'transparent',
            borderColor: '#c44ad4',
            // pointBorderColor: 'transparent',
            // pointBorderWidth: 4,
            // tension: 0.5
        }]
    }

    const option = {
        elements: {
            center: {
              fill: ['rgba(245, 217, 140, 0.5)','rgba(32, 55, 24, 0.5)'] // set the center fill color with 50% opacity
            },
          },
        responsive: true,
        // plugins: {
        //     legend: {
        //         position: 'top',
        //       },
        //       title: {
        //         display: true,
        //         text: 'Visitor Chart'
        //       }
        // }
        // scales: {
        //     x: {
        //         grid: {
        //             display: false
        //         }
        //     },
        //     y:{
        //         min: 2,
        //         max: 10,
        //         ticks: {
        //             stepSize: 2,
        //             callback: (value) => value + 'K'
        //         },
        //         grid:{
        //             borderDash: [10]
        //         }

        //     }
        // }
    }

    return (
        <div>
            <Radar data={data} options={option}></Radar>
        </div>
    )
}

export default RadarChart