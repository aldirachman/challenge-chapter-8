import {Line} from 'react-chartjs-2'

import {
    Chart as ChartJS,
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement
} from 'chart.js'

ChartJS.register(
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement
)

function LineChart(){
    const data = {
        labels: ["Mar 1","Mar 2","Mar 3", "Mar 4","Mar 5","Mar 6"],
        datasets:[{
            label: "Men",
            data: [8,7,6,7,8,7,5,6],
            backgroundColor: '#7ed3ed',
            borderColor: '#7ed3ed',
            pointBorderColor: '#7ed3ed',
            pointBorderWidth: 3,

        },
        {
            label: "Women",
            data: [6,5,9,6,7,7,9,10],
            backgroundColor: '#c44ad4',
            borderColor: '#c44ad4',
            pointBorderColor: '#c44ad4',
            pointBorderWidth: 3,
        }]
    }

    const option = {
        // responsive: true,
        // plugins: {
        //     legend: {
        //         position: 'top',
        //       },
        //       title: {
        //         display: true,
        //         text: 'Visitor Chart'
        //       }
        // },
        // scales: {
        //     x: {
        //         grid: {
        //             display: false
        //         }
        //     },
        //     y:{
        //         min: 2,
        //         max: 10,
        //         ticks: {
        //             stepSize: 2,
        //             callback: (value) => value + 'K'
        //         },
        //         grid:{
        //             borderDash: [10]
        //         }

        //     }
        // }
    }

    return (
        <div>
            <Line data={data} options={option}></Line>
            <Line data={data} options={option}></Line>
        </div>
    )
}

export default LineChart