import {Pie} from 'react-chartjs-2'

import {
    Chart as ChartJS,
    Tooltip,
    Legend,
    ArcElement
} from 'chart.js'

ChartJS.register(
    Tooltip,
    Legend,
    ArcElement
)

function PieChart(){
    const data = {
        labels: ["Januari","Februari","Maret", "April","Mei","Juni"],
        datasets:[{
            data: [8,7,6,5,9,3],
            backgroundColor: ['#c44ad4','#7ed3ed','#a8d977','#debe73','#d66b8b','#e09265'],
            borderColor: '#7ed3ed',
            // pointBorderColor: 'transparent',
            // // pointBorderWidth: 4,
            // // tension: 0.5
        }]
    }

    const option = {
        elements: {
            center: {
              fill: ['rgba(245, 217, 140, 0.5)'] // set the center fill color with 50% opacity
            },
          },
        // responsive: true,
        // plugins: {
        //     legend: {
        //         position: 'top',
        //       },
        //       title: {
        //         display: true,
        //         text: 'Visitor Chart'
        //       }
        // }
        // scales: {
        //     x: {
        //         grid: {
        //             display: false
        //         }
        //     },
        //     y:{
        //         min: 2,
        //         max: 10,
        //         ticks: {
        //             stepSize: 2,
        //             callback: (value) => value + 'K'
        //         },
        //         grid:{
        //             borderDash: [10]
        //         }

        //     }
        // }
    }

    return (
        <div>
            <Pie data={data} options={option}></Pie>
        </div>
    )
}

export default PieChart