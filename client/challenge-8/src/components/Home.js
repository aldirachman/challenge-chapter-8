import React, { Fragment } from 'react'
import {Button, Table} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import Players from './Players'
import {Link, useNavigate} from 'react-router-dom'
import Sidebar from './Sidebar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import LineChart from './charts/LineChart'
import RadarChart from './charts//RadarChart'
import PieChart from './charts/PieChart'



function Home(){

    let history = useNavigate()

    const handleDelete = (id) => {
        var index = Players.map(function(e) {
            return e.id
        }).indexOf(id)

        Players.splice(index,1)

        history('/')

    }

    const handleEdit = (id, username, email, experience) => {
        localStorage.setItem('username',username)
        localStorage.setItem('email',email)
        localStorage.setItem('experience',experience)
        localStorage.setItem('id',id)
    }

    return(
        <>
            <Container fluid className='width-100 m-0 p-0' style={{backgroundColor:'#ebebeb'}}>
                
                <Row>
                    <Col md="auto">
                        <Sidebar/>
                    </Col>
                    <Col>
                        <Row className='p-0'>
                        <h1 className='ps-5 pt-3 text-start'>Dashboard</h1>
                            <Col className='p-0'>
                                <Container className='p-2 align-center' style={{width:'300px', height:'400px',marginTop:'50px', backgroundColor:'white', borderRadius:'10px'}}>
                                    <h3>Visitor Count</h3>
                                    <LineChart/>    
                                </Container>
                            </Col>
                            <Col className='p-0'>
                                <Container className='p-2' style={{width:'300px', height:'400px', marginTop:'50px', backgroundColor:'white', borderRadius:'10px'}}>
                                    <h3>Visitor Gender</h3>
                                    <RadarChart style={{height:'300px'}}/>    
                                </Container>
                            </Col>
                            <Col className='p-0'>
                                <Container className='p-2' style={{width:'300px', height:'400px', marginTop:'50px', backgroundColor:'white', borderRadius:'10px'}}>
                                    <h3>Visitor Sector</h3>
                                    <PieChart style={{height:'300px'}}/>    
                                </Container>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>

           
            
        </>
    )
}

export default Home