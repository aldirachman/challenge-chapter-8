import React, {useState, useEffect} from 'react'
import {Button, Form} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import Players from './Players'
import {v4 as uuid} from 'uuid'
import {Link, useNavigate} from 'react-router-dom'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Sidebar from './Sidebar';

function Edit(){
    const[id, setId] = useState('')
    const[username, setUsername] = useState('')
    const[email, setEmail] = useState('')
    const[experience, setExperience] = useState('')

    let history = useNavigate()

    var index = Players.map(function(e){
        return e.id
    }).indexOf(id)

    const handleSubmit = (e) => {
        e.preventDefault()

        let a = Players[index]
        a.username = username
        a.email = email
        a.experience = experience

        history('/player')
    }

    useEffect(() => {
        setUsername(localStorage.getItem('username'))
        setEmail(localStorage.getItem('email'))
        setExperience(localStorage.getItem('experience'))
        setId(localStorage.getItem('id'))
    },[])

    return (
        <div>
            <Container fluid className='width-100 m-0 p-0' style={{backgroundColor:'#ebebeb'}}>
                <Row>
                    <Col md="auto">
                        <Sidebar/>
                    </Col>
                    <Col>
                    <h1 className='ps-5 pt-3 text-start'>Edit Player</h1>
                    <Form className='d-grid gap-2' style={{margin:"3rem"}}>
                        <Form.Group className='mb-1 text-start' controlId='formUsername'>
                            <label htmlFor='username' className='form-label text-left !important' >
                                Username
                            </label>
                            <Form.Control type='text' placeholder='Enter Username' value={username} required onChange={(e) => setUsername(e.target.value)}>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group className='mb-1 text-start' controlId='formEmail'>
                            <label htmlFor='email' className='form-label text-left !important' >
                                Email
                            </label>
                            <Form.Control type='email' placeholder='Enter Email' value={email} required onChange={(e) => setEmail(e.target.value)}>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group className='mb-1 text-start' controlId='formExperience'>
                            <label htmlFor='email' className='form-label text-left !important' >
                                Email
                            </label>
                            <Form.Control type='text' placeholder='Enter Experience' value={experience} required onChange={(e) => setExperience(e.target.value)}>
                            </Form.Control>
                        </Form.Group>
                        <Button onClick={(e) => handleSubmit(e)} type='submit'>Submit</Button>
                    </Form>
                    </Col>
                </Row>
            </Container>

            
        </div>
    )
}

export default Edit