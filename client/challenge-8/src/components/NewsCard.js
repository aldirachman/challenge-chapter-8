import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import React, { useState, useEffect } from 'react';

function NewsCard(props) {
  const [img, setImg] = useState(props.img);

  return (
    <Card style={{ width: '18rem', }}>
      <Card.Img style={{height:'100px', objectFit: 'cover', objectPosition: 'center'}} variant="top" src={img} />
      <Card.Body>
        <Card.Title>{props.title}</Card.Title>
        <Card.Text>{props.text}</Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
    </Card>
  );
}

export default NewsCard;
