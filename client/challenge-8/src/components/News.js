import React, { Fragment } from 'react'
import {Button, Table} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import Sidebar from './Sidebar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import NewsCard from './NewsCard'
import Carousel from './Carousel'

function News(){
    return(
        <>
            <Container fluid className='width-100 m-0 p-0'>
                <Row>
                    <Col md="auto">
                        <Sidebar/>
                    </Col>
                    <Col>
                        <Row style={{height:'320px', margin:'2rem',borderRadius:'10px'}}>
                            <Col>
                                <Carousel/>
                            </Col>
                        </Row>
                        <Row style={{margin:'2rem'}}>
                            <Col className="d-flex justify-content-center">
                                <NewsCard title="The Origin of Air Jordan 1 'Chicago'" text="Your text value here" img="/jordan.webp" />   
                            </Col>
                            <Col className="d-flex justify-content-center">
                                <NewsCard title="Lebron James pass Kareem Abdul Jabbar" text="Your text value here" img="/lebron2.webp" />   
                            </Col>
                            <Col className="d-flex justify-content-center">
                                <NewsCard title="Kyrie on being traded to the Dallas Mavericks" text="Your text value here" img="/kyrie.webp" />   
                            </Col>   
                        </Row>
                        
                    </Col>
                </Row>
            </Container>
           
            
        </>
    )
}

export default News
