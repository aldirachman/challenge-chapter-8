import React, { Fragment } from 'react'
import {Button, Table} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import Players from './Players'
import {Link, useNavigate} from 'react-router-dom'
import Sidebar from './Sidebar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function ManagePlayer(){

    let history = useNavigate()

    const handleDelete = (id) => {
        if (window.confirm('Are you sure you want to delete this player?')) {
            var index = Players.map(function(e) {
                return e.id
            }).indexOf(id)
    
            Players.splice(index,1)
    
            history('/player')
        }

    }

    const handleEdit = (id, username, email, experience) => {
        localStorage.setItem('username',username)
        localStorage.setItem('email',email)
        localStorage.setItem('experience',experience)
        localStorage.setItem('id',id)
    }

    return(
        <>
            <Container fluid className='width-100 m-0 p-0'>
                <Row>
                    <Col md="auto">
                        <Sidebar/>
                    </Col>
                    <Col>
                    <h1 className='ps-5 pt-3 text-start'>Manage Player</h1>
                        <Fragment>
                            <div style={{margin:"3rem"}}>
                            <Link className='d-grid gap-2 mb-5' to='/create'>
                                <Button size='lg'>Create</Button>
                            </Link>
                            <Table striped bordered hover size="sm">
                                <thead>
                                    <tr>
                                        <th>
                                            Username
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Experience
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        Players && Players.length > 0
                                        ?
                                        Players.map((item) => {
                                            return(
                                            <tr>
                                                <td>
                                                    {item.username}
                                                </td>
                                                <td>
                                                    {item.email}
                                                </td>
                                                <td>
                                                    {item.experience}
                                                </td>
                                                <td>
                                                    <Link to={'/edit'}>
                                                        <Button onClick={() => handleEdit(item.id, item.username, item.email, item.experience)}>Edit</Button>
                                                    </Link>
                                                    &nbsp;
                                                    <Button onClick={() => handleDelete(item.id)}>Delete</Button>
                                                </td>
                                            </tr>
                                            )
                                        })
                                        :
                                        "No Data Available"
                                    }
                                </tbody>
                            </Table>
                            <br>
                            </br>
                            
                        </div>
                        </Fragment>
                    </Col>
                </Row>
            </Container>
           
            
        </>
    )
}

export default ManagePlayer