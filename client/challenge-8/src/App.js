import logo from './logo.svg';
import './App.css';
import Add from './components/Add'
import Edit from './components/Edit'
import Home from './components/Home'
import News from './components/News'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import ManagePlayer from './components/ManagePlayer';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path='/' element={<Home/>}></Route>
          <Route path='/player' element={<ManagePlayer/>}></Route>
          <Route path='/create' element={<Add/>}></Route>
          <Route path='/edit' element={<Edit/>}></Route>
          <Route path='/news' element={<News/>}></Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
