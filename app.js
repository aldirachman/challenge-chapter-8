const express = require('express')
const app = express()
const cors = require('cors')
const apiRouter = require('./server/routes')
const errorHandler = require('./server/middlewares/errorHandler')
const PORT = process.env.PORT || 4000
const swaggerUI = require('swagger-ui-express')
const openApiJson = require('./documentation.json')
const path = require('path')

// middlewares
app.use(cors())
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(errorHandler)

app.use(express.json())
app.set('view engine', 'ejs')

app.use(express.static(path.join(__dirname, '/public')));

/**
 * @Routes /api
 * entrypoint for all API routes
 */
app.use("/api", apiRouter)

const dashboardRouter = require('./server/routes/')
app.use("/dashboard", dashboardRouter)

app.get('/adduser', (req, res) => {
  res.render('dashboard/adduser')
})

app.get('/', async (req, res) => {
  const resp = await fetch('http://localhost:4000/api/v1/players')
  const data = await resp.json()
  console.log(data)
  res.render('dashboard/listuser', {players: data })
})

app.get('/:id', async (req, res) => {
  const userId = req.params.id;
  // Fetch the user data
  const userResp = await fetch(`http://localhost:4000/api/v1/players/${userId}`);
  const userData = await userResp.json();
  // Fetch the user biodata

  // Render the dashboard-userdetail view, passing in the user and userBiodata data
  res.render('dashboard/userdetail', {players: userData});
});

app.use('/api/doc', swaggerUI.serve, swaggerUI.setup(openApiJson))

app.listen(PORT, () => {
  console.log(`Listening on http://localhost:${PORT}`)
})